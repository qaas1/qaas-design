# Runtime Runner 
The program that compiles the user projects when using the qaas run command will need to have support for dotnet 3.1 and dotnet 6 at the very least and can compile them using the following options:
* run a dotnet build process
* use the following nugets to programtically build the user's project: 
    * Microsoft.Build.Locator
    * Microsoft.Build
    * Microsoft.CodeAnalysis.CSharp
    * Microsoft.CodeAnalysis.Workspaces

```c# 
using Microsoft.Build.Locator;

MSBuildLocator.RegisterDefaults();

using Microsoft.CodeAnalysis.MSBuild;

// Load the project
using MSBuildWorkspace workspace = MSBuildWorkspace.Create();
Project project = await workspace.OpenProjectAsync("path/to/your/project.csproj");

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Emit;

// Build the project
EmitResult result = await project.GetCompilationAsync().EmitAsync("path/to/output/directory");

if (result.Success)
{
    // Build succeeded
}
else
{
    // Build failed
}
```