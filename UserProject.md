# User Project

The user's project will be written in dotnet core (should support multiple versions, lowest starting at 3.1) and will have to contain a reference to the `QAAS.SDK` nuget at the matching version to the QAAS version it runs with.

Will be able to contain one of or both of the following functionalities:

* **Generators** List of `Generator` object that represents a way to generate data according to the qaas interface and may be transferred to the CI.Communication project for data generation in certain sessions.

* **QA.DIY** NUnit Test Project that receives a `SessionOutput` list and a path to a yaml configuration file and may use them as it pleases to peform DIY qa steps.

possible nuget seperate from the QAAS.SDK that will have `Generator` list and `SessionOutput` list that the loaded generators and session output results will be kept in and shared across the different projects with. can also contain another object (`IDictionary<string, object>`) that the sole purpose of is to pass data between the Generators and the QA.DIY of the same project.

## Generators implementation possibilities
* Will look for a certain class inhereiting from a known interface present in the `QAAS.SDK` and then construct it and invoke the relevant function or access the relevant property in that interface to receive the generators.

* Will look for a function/property with a configured given name at a configured given class that will give the generators.

* one of the above but instead of returning the generators wil put them in a static `Generators` list present in the QAAS.SDK or another nuget shard between the user project and the qaas framework itself.


## QA.DIY implementation possiblities
* Will be loaded and compiled to dlls, the build results (dlls and resources) could be kept in a seperate directory that the project builder could simply invoke with `NUnitLite` as a seperate entity from the rest of the qaas framework, will need to load SessionOutput list into that built assembly somehow, could the relevant dlls be given this object before the tests will be ran? if not will have to be given through S3/FileSystem/IPC

