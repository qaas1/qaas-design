# Dependencies
Every project written in dotnet 3.1 with c# 8 unless specified otherwise.


* **QAAS Step Nugets** (multiple projects in this category) - nugets containing functionality used across all QA Steps / Ci.Communication repeatedly.

* **CI.Communication** - runs communication sessions with either the user given generators (configured to the CLI as sources to load) or Raw data from s3/filesystem and returns the session outputs either as json files in s3/filesystem or as a list of session output objects.

* **QA Steps** (multiple projects in this category all running in parallel) - runs assertions on the session outputs and returns test results in allure format to a folder at configured path. the steps only know to get a sessionoutput list.

* **QAAS Steps** - Contains a reference to all qa steps and to ci.communication and organizes them in an easily runnable structured object from which they can each be called and ran with relevant configuration. 

* **QAAS Custom Project Builder** Dotnet 6 C# 10 (should support both running dotnet 3.1 and dotnet 6 projects) - Contains functionality to load a user project's generators/custom qa steps Using the `QAAS.SDk` in a way they can be called and ran with an **independent** assembly context from the rest of the project that can pass objects(representing the custom qa steps or generators) to the rest of the framework to use with certain set objects. (Can run the custom qa step as a completely independent DLL using `NUnitLite` and return the generator as a loaded dll generator using the Generator schema and assembly.loadcontext() currently present in `ci.communication`)

* **QAAS.Cli** Dotnet 6 C# 10 - CLI that runs/lints/templates a qaas configuration file.

* **QAAS.SDK** - Contains the following qaas things that must be present in both the user's project and the already made generic projects -
    * SessionOutput object
    * Generator object (Using WrapIt wrapper for shared functionality)
    * QAAS Step interface (Interface representing any qaas step - ci, qa or custom that can be ran with a given full path to a configuration file)
    * Serilog logger relevant libraries ??? should possibly be in a seperate non mendatory nuget? depends if i want to pass the DIY a logger

* **QAAS.QA STEP RUNNER** - Contains functionality to run a qa step with `NunitLite` and put allure results in a certain configured path.

* **QAAS.Nugets.Configuration** - contains configuration related functionality for all qaas projects to use, uses `microsoft.extensions.configurations` as its base configuration nuget, making it the base configuration nuget for all qaas projects.

* **Assetions** Contains AssetionHub which references built in Generic Assertion projects (which are a converted none NUnit test qa steps that will be run from the CLI in an actual NUnit Test Function)

* **Assertions.DIY** The user given assertions that must only implement the IAssertion interface and can be loaded into a qaas run from code.


